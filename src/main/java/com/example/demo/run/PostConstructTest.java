package com.example.demo.run;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PostConstructTest {

    @PostConstruct
    public void start(){
        System.out.println("---PostConstruct start---");
    }


}
